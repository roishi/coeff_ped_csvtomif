class pycolor:
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    PURPLE = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    RETURN = '\033[07m'
    ACCENT = '\033[01m'
    FLASH = '\033[05m'
    RED_FLASH = '\033[05;41m'
    END = '\033[0m'

# import python standard library
import re
import os
import sys
import time
import pprint
import random
import copy
import math
import csv
import sqlite3

import optparse
parser = optparse.OptionParser()
parser.add_option('-v', '--verbose', dest = 'verbose', action = 'store_true', default = False, help = 'Verbose False by default')
parser.add_option('-r', '--rootdir', dest = 'root_directory', action = 'store', default = '/home/users/oishi/LATOME_FW_v3.2.1/LATOME/', help = 'LATOME repository root directory')
parser.add_option('-s', '--swap', dest = 'swap', action = 'store_true', default = False, help = 'Swap coefficient a and b to check outputs from Saturation detection block')
(options, args) = parser.parse_args()

if os.path.isdir(options.root_directory + 'testbench/'):
    sys.path.append(options.root_directory + 'testbench/')
    # Awsome modules
    from IntBitVector import *
    from mif_file import *
else:
    print('You need to specify valid LATOME root directory\n%s was not available' % (options.root_directory + 'testbench/'))
    sys.exit(1)

USER_CODE_STREAM_NB_SAMPLES = 6
USER_PEDESTAL_HARDPOINT = 3
USER_HARDPOINT_IN_FIR_FILTER = 8
USER_HARDPOINT_IN_SATURATION_DETECTION = 6

USER_CODE_STREAM_NB_SAMPLES_WIDTH = 3
USER_CODE_STREAM_NB_SAMPLES_IN_MEMORY = int(pow(2, USER_CODE_STREAM_NB_SAMPLES_WIDTH))
USER_FILTERING_IN_FIR_FILTER_TAPS_NB = 4
USER_FILTERING_IN_SATURATION_DETECTION_TAPS_NB = USER_FILTERING_IN_FIR_FILTER_TAPS_NB
USER_COEFFICIENT_WIDTH = 14
USER_PEDESTAL_WIDTH = 14

DET_int_to_name = {'EMB': 0, 'EMEC': 1, 'HEC': 2, 'FCAL': 3}
AC_int_to_name = {'A': 1, 'C': -1}

class Convertcsvtomif:

    """
    Convert text file to MIF file.
    Note
    --> Swap a_i and b_i with '-s'
    --> Fill any coefficients in any position
    Todo
    --> Need to find better methods to search LATOME root directory
    --> Need to find a method to load constants in /src/user/user.py
    --> Supercell names are not really unique, they are unique for sub-detector, side and quadrant. How can we know which is which??
    """

    def __init__(self, inputfile):
        self.inputfile = inputfile
        self.infodict_fromcsv = {}
        self.infodict_frompu = {}
        self.Allinfodict = {} # You print out this variable when you messed up.

        if options.swap:
            print('!! WARNING !!\n  You pass -s as an argument and this argument swaps coefficients a and b. Is it OK??')

    def Readfile(self):
        print('Configureation')
        store_key_and_index = {}
        with open(self.inputfile) as f:
            reader = csv.reader(f)
            csv_key_line = True
            for row in reader:
                if not '#' in row[0]:
                    if csv_key_line:
                        csv_key_line = False
                        for index in range(len(row)):
                            store_key_and_index[index] = row[index]
                    else:
                        infodict_fromcsv_inner = {'a': [], 'b': [], 'pedestal': 0}
                        for index in range(1, len(row)):
                            if 'ped' in store_key_and_index[index]:
                                ped_tmp = int(pow(2, USER_PEDESTAL_HARDPOINT) * float(row[index]))
                                infodict_fromcsv_inner['pedestal'] = ped_tmp
                            elif 'a' in store_key_and_index[index]:
                                coef_tmp = int(pow(2, USER_HARDPOINT_IN_FIR_FILTER - USER_PEDESTAL_HARDPOINT) * float(row[index]))
                                infodict_fromcsv_inner['a'].append(coef_tmp)
                            elif 'b' in store_key_and_index[index]:
                                coef_tmp = int(pow(2, USER_HARDPOINT_IN_SATURATION_DETECTION - USER_PEDESTAL_HARDPOINT) * float(row[index]))
                                infodict_fromcsv_inner['b'].append(coef_tmp)
                        self.infodict_fromcsv[row[0]] = infodict_fromcsv_inner
                else:
                    line = ''
                    if not len(row) == 1:
                        for i, element in enumerate(row):
                            line += element
                            if not i == len(row)-1:
                                line += ','
                    else:
                        line = row[0]
                    line = line.replace('#', '')
                    line = line.replace(' ', '')
                    print(line)
                    if '=' in line and line.count('=') == 1:
                        rhs_lhs = line.split('=')
                        setattr(self, rhs_lhs[0], rhs_lhs[1])

    def GetLATOMEID_SCNAME(self):
        database_path = '/afs/cern.ch/user/l/larmon/public/prod/LArIdtranslator/LArId.db'
        conn = sqlite3.connect(database_path)
        conn.row_factory = sqlite3.Row
        c = conn.cursor()

        database_execute_command = 'select '
        target_list = ['SCNAME', 'LATOME', 'DET', 'AC'] # Add a key here if you need
        for i, target in enumerate(target_list):
            database_execute_command += target
            if not i == len(target_list)-1:
                database_execute_command += ','
        database_execute_command += ' from LARID where SC_ONL_ID = ?'

        key_sconlineid = self.infodict_fromcsv.keys()
        for key in key_sconlineid:
            fundamental_variables = (int(key, 0),)
            c.execute(database_execute_command, fundamental_variables)
            fetch_all = c.fetchall()
            fetch_one_dict = {}
            for target in target_list:
                fetch_one_dict[target] = ''
            for fetch in fetch_all:
                for target in target_list:
                    if fetch_one_dict[target] == '':
                        fetch_one_dict[target] = fetch[target_list.index(target)]
                    else:
                        if fetch_one_dict[target] != fetch[target_list.index(target)]:
                            print('SC_ONL_ID %s has at least two different %s, check \'%s\'' % (key, target_list[index], 'https://atlas-larmon.cern.ch/LArIdtranslator/'))
                            sys.exit(2)
                        else:
                            pass

            if not fetch_one_dict['LATOME'] in self.infodict_fromcsv:
                self.infodict_fromcsv[fetch_one_dict['LATOME']] = {}
                self.infodict_fromcsv[fetch_one_dict['LATOME']][fetch_one_dict['SCNAME']] = self.infodict_fromcsv[key]
                del self.infodict_fromcsv[key]
            else:
                self.infodict_fromcsv[fetch_one_dict['LATOME']][fetch_one_dict['SCNAME']] = self.infodict_fromcsv[key]
                del self.infodict_fromcsv[key]

        if options.verbose:
            pprint.pprint(self.infodict_fromcsv)

        c.close()
        conn.close()

    def ReadPUList(self):
        LATOME_Mappings = []
        if not 'Mapping' in self.__dict__:
            print('You did\'t specify mapping name.')
            sys.exit(3)
        else:
            if ',' in self.__dict__['Mapping']:
                LATOME_Mappings = self.__dict__['Mapping'].split(',')
            else:
                LATOME_Mappings.append(self.__dict__['Mapping'])

        for mapping in LATOME_Mappings:
            if 'LATOME_' in mapping:
                mapping = mapping.replace('LATOME_', '')
            if mapping != 'EMBA_12':
                mapping = 'EMBA-EMECA_12'
            self.infodict_frompu[mapping] = {}
            pu_list_path = options.root_directory + 'LATOME/mapping/production/data/%s/pu_list_latome.txt' % (mapping)
            if os.path.isfile(pu_list_path):
                print('PU List was detected at\n  %s' % (pu_list_path))
            else:
                print('PU List was missing...\n  %s' % (pu_list_path))
                sys.exit(4)
            pu_lines = []
            with open(pu_list_path) as f:
                pu_lines = f.readlines()
            for line in pu_lines:
                line_list = line.split()
                streamid = line_list[0].replace('PU', '')
                self.infodict_frompu[mapping][streamid] = {}
                scid = 0
                for index in range(1, len(line_list)):
                    if not line_list[index][0] == '(':
                        self.infodict_frompu[mapping][streamid][scid] = {'SCNAME': line_list[index], 'a': None, 'b': None, 'pedestal': None}
                        scid += 1

        for mapping in self.infodict_frompu:
            for streamid in self.infodict_frompu[mapping]:
                if not len(self.infodict_frompu[mapping][streamid]) == USER_CODE_STREAM_NB_SAMPLES:
                    print('Number of SC in stream %s is inconsistent, (expected %d but there are %d)'
                          % (streamid, len(self.infodict_frompu[mapping][streamid]), USER_CODE_STREAM_NB_SAMPLES))
                    sys.exit(3)

        if options.verbose:
            pprint.pprint(self.infodict_frompu)

    def DictParser(self):
        for mapping in self.infodict_frompu:
            for streamid in self.infodict_frompu[mapping]:
                for scindex in self.infodict_frompu[mapping][streamid]:
                    if not self.infodict_frompu[mapping][streamid][scindex]['SCNAME'] == 'GND':
                        fullscid_list = self.infodict_frompu[mapping][streamid][scindex]['SCNAME'].split('_')
                        scid = '%s_%s' % (fullscid_list[1], fullscid_list[2])
                        latome_good, sc_good = self._Find_LATOME_COEF_PED(scid)
                        if not latome_good == 'dummy':
                            if not latome_good in self.Allinfodict:
                                self.Allinfodict[latome_good] = self.infodict_frompu[mapping]
                            for constant in self.infodict_frompu[mapping][streamid][scindex]:
                                if not constant == 'SCNAME':
                                    self.Allinfodict[latome_good][streamid][scindex][constant] = sc_good[constant]

        for latome in self.Allinfodict:
            for streamid in self.Allinfodict[latome]:
                for scindex in self.Allinfodict[latome][streamid]:
                    if self.Allinfodict[latome][streamid][scindex]['SCNAME'] == 'GND':
                        _, sc_dead = self._Find_LATOME_COEF_PED_dummy()
                        for constant in self.Allinfodict[latome][streamid][scindex]:
                            if not constant == 'SCNAME':
                                 self.Allinfodict[latome][streamid][scindex][constant] = sc_dead[constant]

        if options.verbose:
            pprint.pprint(self.Allinfodict)


    def _Find_LATOME_COEF_PED(self, target_scid):
        print(target_scid)
        for latome in self.infodict_fromcsv:
            for scid in self.infodict_fromcsv[latome]:
                if scid == target_scid:
                    return latome, self.infodict_fromcsv[latome][scid]

        print('self.infodict_fromcsv doesn\'t have coef/ped information for %s\nPlease check if your input file is good.' % (target_scid))
        return self._Find_LATOME_COEF_PED_dummy()

    def _Find_LATOME_COEF_PED_dummy(self, target_scid = 'GND'):
        latome = 'dummy'
        dummy_info = {'a': [0, 0, 0, 1], 'b': [0, 0, 0, 1], 'pedestal': 0}
        return latome, dummy_info

    def MakeMIF(self):
        self._ZeroFill_for_deadSC()

        depth_fir = int(pow(2, math.ceil(math.log(USER_CODE_STREAM_NB_SAMPLES_IN_MEMORY * (USER_FILTERING_IN_FIR_FILTER_TAPS_NB + 1), 2))))
        depth_sat = int(pow(2, math.ceil(math.log(USER_CODE_STREAM_NB_SAMPLES_IN_MEMORY * (USER_FILTERING_IN_SATURATION_DETECTION_TAPS_NB + 1), 2))))

        for latome in self.Allinfodict:
            for streamid in self.Allinfodict[latome]:
                streamid_int = int(streamid) - 1
                coeffile_fir_name = 'output/%s/coefficient_fir_init_file.%02d.mif' % (latome, streamid_int)
                coeffile_sat_name = 'output/%s/coefficient_sat_init_file.%02d.mif' % (latome, streamid_int)
                if not os.path.exists('output/%s/' % (latome)):
                    os.makedirs('output/%s/' % (latome))
                print(coeffile_fir_name)
                fir_miffile = MIFFile(coeffile_fir_name, MIFFile.MODE_WRITE, depth = depth_fir, width = USER_COEFFICIENT_WIDTH)
                sat_miffile = MIFFile(coeffile_sat_name, MIFFile.MODE_WRITE, depth = depth_fir, width = USER_COEFFICIENT_WIDTH)
                for coef_index in range(USER_FILTERING_IN_FIR_FILTER_TAPS_NB):
                    for scindex in range(USER_CODE_STREAM_NB_SAMPLES_IN_MEMORY):
                        if scindex < len(self.Allinfodict[latome][streamid]):
                            if not options.swap: # Usual case
                                fir_miffile.dump(IntBitVector(size = USER_COEFFICIENT_WIDTH, intVal = self.Allinfodict[latome][streamid][scindex]['a'][coef_index]))
                                sat_miffile.dump(IntBitVector(size = USER_COEFFICIENT_WIDTH, intVal = self.Allinfodict[latome][streamid][scindex]['b'][coef_index]))
                            else: # Special case
                                fir_miffile.dump(IntBitVector(size = USER_COEFFICIENT_WIDTH, intVal = self.Allinfodict[latome][streamid][scindex]['b'][coef_index]))
                                sat_miffile.dump(IntBitVector(size = USER_COEFFICIENT_WIDTH, intVal = self.Allinfodict[latome][streamid][scindex]['a'][coef_index]))
                        else:
                            fir_miffile.dump(IntBitVector(size = USER_COEFFICIENT_WIDTH, intVal = 0))
                            sat_miffile.dump(IntBitVector(size = USER_COEFFICIENT_WIDTH, intVal = 0))
                for scid in self.Allinfodict[latome][streamid]:
                    fir_miffile.dump(IntBitVector(size = USER_PEDESTAL_WIDTH, intVal = self.Allinfodict[latome][streamid][scid]['pedestal']))
                    sat_miffile.dump(IntBitVector(size = USER_PEDESTAL_WIDTH, intVal = self.Allinfodict[latome][streamid][scid]['pedestal']))
                fir_miffile.close()
                sat_miffile.close()

        if options.verbose:
            pprint.pprint(self.Allinfodict)

    def _ZeroFill_for_deadSC(self):
        for latome in self.Allinfodict:
            for streamid in self.Allinfodict[latome]:
                for scid in self.Allinfodict[latome][streamid]:
                    if self.Allinfodict[latome][streamid][scid]['pedestal'] is None:
                        _, dummy_info = self._Find_LATOME_COEF_PED_dummy()
                        for constant in dummy_info:
                            self.Allinfodict[latome][streamid][scid][constant] = dummy_info[constant]

    def Main(self):
        self.Readfile()
        self.GetLATOMEID_SCNAME()
        self.ReadPUList()
        self.DictParser()
        self.MakeMIF()


if __name__ == '__main__':
    start_time = time.time()

    inputfile = 'ofc_test_20201205_long.csv'
    obj = Convertcsvtomif(inputfile)
    print(obj.__doc__)
    obj.Main()

    end_time = time.time()
    print('Elapsed time: %.3f' % (end_time - start_time))
    
